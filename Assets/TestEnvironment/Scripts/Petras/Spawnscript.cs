using UnityEngine;
using System;
using System.Collections;

public class Spawnscript : MonoBehaviour {

	public Transform playerPrefab;
	public ArrayList playerScripts;

	void OnServerInitialized(){
		//Spawn a player for the server itself
		Spawnplayer(Network.player);
	}

	void OnPlayerConnected(NetworkPlayer newPlayer) {
		//A player connected to me(the server)!
		Spawnplayer(newPlayer);
	}	

	
	void OnPlayerDisconnected(NetworkPlayer player) {
		Debug.Log("Clean up after player " + player);

		foreach(Playerscript script in playerScripts){
			if(player==script.owner){//We found the players object
				Network.RemoveRPCs(script.gameObject.networkView.viewID);//remove the bufferd SetPlayer call
				Network.Destroy(script.gameObject);//Destroying the GO will destroy everything
				playerScripts.Remove(script);//Remove this player from the list
				break;
			}
		}
		
		//Remove the buffered RPC call for instantiate for this player.
		int playerNumber = Convert.ToInt32(player+"");
		Network.RemoveRPCs(Network.player, playerNumber);
		
		
		// The next destroys will not destroy anything since the players never
		// instantiated anything nor buffered RPCs
		Network.RemoveRPCs(player);
		Network.DestroyPlayerObjects(player);
	}

	void OnDisconnectedFromServer(NetworkDisconnection info) {
		Debug.Log("Resetting the scene the easy way.");
		Application.LoadLevel(Application.loadedLevel);	
	}
	
	void Spawnplayer(NetworkPlayer newPlayer){
		//Called on the server only
		
		int playerNumber = Convert.ToInt32(newPlayer+"");
		//Instantiate a new object for this player, remember; the server is therefore the owner.
		Transform myNewTrans = (Transform)Network.Instantiate(playerPrefab, transform.position, transform.rotation, playerNumber);
		
		//Get the networkview of this new transform
		NetworkView newObjectsNetworkview = myNewTrans.networkView;
		
		//Keep track of this new player so we can properly destroy it when required.
		playerScripts.Add(myNewTrans.GetComponent<Playerscript>());
		
		//Call an RPC on this new networkview, set the player who controls this player
		newObjectsNetworkview.RPC("SetPlayer", RPCMode.AllBuffered, newPlayer);//Set it on the owner
	}
	

	// Use this for initialization
	void Start () {
		playerScripts = new ArrayList();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
