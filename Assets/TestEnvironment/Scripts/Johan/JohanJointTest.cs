using UnityEngine;
using System.Collections;

public class JohanJointTest : MonoBehaviour
{
	
	GameObject obj1 = null;
	GameObject obj2 = null;
	
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		
		if (Input.GetKeyDown(KeyCode.B))
			spawnSomeBoxes();
		
		
		
		if (Input.GetMouseButtonDown (0)) {	
			Rigidbody body = getBodyFromRay ();
			if (body != null) {
				obj1 = body.gameObject;	
				print ("found object 1");
			}
		}
		if (Input.GetMouseButtonDown (1)) {	
			Rigidbody body = getBodyFromRay ();
			if (body != null) {
				obj2 = body.gameObject;	
				print ("found object 2");
			}
		}
		
		//If two objects are selected. do joint
		if (obj1 != null && obj2 != null) {
		
			print ("JOINING");
			
			//Add configurablejoint component
			obj1.AddComponent ("ConfigurableJoint");
			ConfigurableJoint joint = (ConfigurableJoint)obj1.GetComponent ("ConfigurableJoint");
			joint.connectedBody = obj2.rigidbody;
			
			//lock the movement between the objects
			joint.xMotion = ConfigurableJointMotion.Locked;
			joint.yMotion = ConfigurableJointMotion.Locked;
			joint.zMotion = ConfigurableJointMotion.Locked;
			joint.angularXMotion = ConfigurableJointMotion.Locked;
			joint.angularYMotion = ConfigurableJointMotion.Locked;
			joint.angularZMotion = ConfigurableJointMotion.Locked;
			
			//Set selection to null again
			obj1 = null;
			obj2 = null;
		}
			
	}
	
	void spawnSomeBoxes ()
	{

		GameObject box;
		float y = 15.0f;
			
		for (float x = -5.0f; x<=5.0f; x += 2f) {
			for (float z = -5.0f; z<=5.0f; z += 2f) {
				box = GameObject.CreatePrimitive (PrimitiveType.Cube);
				//ball.transform.localScale = new Vector3 (0.5f, 0.5f, 0.5f);
				box.transform.position = new Vector3 (x, y, z);
				box.AddComponent("Rigidbody");
				box.rigidbody.interpolation = RigidbodyInterpolation.Interpolate;
			}
		}
		
		
	}
	
	Rigidbody getBodyFromRay ()
	{
		Ray r = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit rHit;
		
		if (Physics.Raycast (r, out rHit)) {
			return rHit.rigidbody;
		}
		
		return null;
	}
	
}
