using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
 * A script for building the building grid and moving blocks around in the grid.
 * Script should be put on an empty gameobject that is placed (on the floor) to indicate where the
 * buildinggrid is positioned.
 * 
 * Author: Johan
 * 
 */

public class BuildingScript : MonoBehaviour {
	
	//The basic building block (unnecessary?)
	public GameObject bbb;
	
	public List<GameObject> blockList;
	public GameObject selectedBlock;
	
	//The size of one side
	private float bbbSize;
	private float stepSize;
	
	//The ghost objects
	private GameObject ghost;
	
	
	//The starting position of the grid.
	private Vector3 origin;
	//The current position in the grid
	private Vector3 currPos;
	
	public int width = 30;
	public int height = 30;
	
	
	
	
	void Start () {
		origin = transform.position; 
		bbbSize = bbb.collider.bounds.size.x;
		origin.y += bbbSize/2;
		currPos = new Vector3(0,0,0);
		
		//
		stepSize = bbbSize / 4;
		
		//Set bbb to be selected object
		selectedBlock = bbb;
		
		print("starting script");
	}
	
	
	void Update () {
		
		//Keys for debugging
		if(Input.GetKeyDown(KeyCode.W))
			moveX(1);
		if(Input.GetKeyDown(KeyCode.S))
			moveX(-1);
		if(Input.GetKeyDown(KeyCode.Q))
			moveY(-1);
		if(Input.GetKeyDown(KeyCode.E))
			moveY(1);
		if(Input.GetKeyDown(KeyCode.A))
			moveZ(-1);
		if(Input.GetKeyDown(KeyCode.D))
			moveZ(1);
		if(Input.GetKeyDown(KeyCode.Alpha1))
			selectBlock(0);
		if(Input.GetKeyDown(KeyCode.Alpha2))
			selectBlock(1);
		if(Input.GetKeyDown(KeyCode.Alpha3))
			selectBlock(2);
		if(Input.GetKeyDown(KeyCode.Space))
			placeBlock(bbb);
		
		drawGhost();
		
		drawGridBounds();
	}
	
	public void moveX(int steps){
		//Don't move outside the grid
		if(currPos.x+steps >= 0 && currPos.x+steps <= width)
			currPos.x +=steps;
	}
	
	public void moveY(int steps){
		if(currPos.y+steps >= 0 && currPos.y+steps <= height)
			currPos.y +=steps;
	}
	
	public void moveZ(int steps){
		if(currPos.z+steps >= 0 && currPos.z+steps <= width)
			currPos.z +=steps;
	}
	
	public void selectBlock(int block){
		
		print("Changing block");
		
		if(blockList[block] != null)
			selectedBlock = blockList[block];
		
		//destroy ghost and set ghost to null so a new ghost is generated
		GameObject.Destroy(ghost);
		ghost = null;
	}
	
	
	/* Spawns a block
	*  TODO Be able to place different objects, check if valid position, place rotated objects.
	*/
	public GameObject placeBlock(GameObject block){
		print ("Placing block");
		return (GameObject)Instantiate(selectedBlock, currentPosInCoords(), transform.rotation);
	}
	
	private void drawGhost(){
		
		//Move the ghost if there is one
		if(ghost != null){
			ghost.transform.position = currentPosInCoords();
		}else{
			//Create new ghost from selected block
			ghost = (GameObject)Instantiate(selectedBlock, currentPosInCoords(), transform.rotation);
			ghost.AddComponent("GhostBlockScript");
		}
		
	}
	
	private Vector3 currentPosInCoords(){
		
		
		Vector3 pos = transform.position;
		
		//Move to origin
		pos.x = origin.x;
		pos.y = origin.y;
		pos.z = origin.z;
		
		//Move to the currentposition in the grid
		pos.x += currPos.x*stepSize;
		pos.y += currPos.y*stepSize;
		pos.z += currPos.z*stepSize;
		
		return pos;
		
	}
	
	private Vector3 posInCoords(int x, int y, int z){
	
		Vector3 p = transform.position;
		
		//Move to origin
		p.x = origin.x;
		p.y = origin.y;
		p.z = origin.z;
		
		//Move to the currentposition in the grid
		p.x += x*stepSize;
		p.y += y*stepSize;
		p.z += z*stepSize;
		
		return p;
		
		
	}
	
	/*
	 * Draw some lines to visualize the bounds of the buildinggrid
	 * 
	 */
	private void drawGridBounds(){
		
		Debug.DrawLine( posInCoords(0,0,0), posInCoords(width,0,0));
		Debug.DrawLine( posInCoords(0,0,0), posInCoords(0,height,0)); 
		Debug.DrawLine( posInCoords(0,0,0), posInCoords(0,0,width)); 
		
		Debug.DrawLine( posInCoords(width,height,width), posInCoords(width,height,0));
		Debug.DrawLine( posInCoords(width,height,width), posInCoords(width,0,width)); 
		Debug.DrawLine( posInCoords(width,height,width), posInCoords(0,height,width));
		
		Debug.DrawLine( posInCoords(width,height,0), posInCoords(0,height,0));
		Debug.DrawLine( posInCoords(width,height,0), posInCoords(width,0,0));
		Debug.DrawLine( posInCoords(width,0,width), posInCoords(width,0,0));
		
		Debug.DrawLine( posInCoords(0,0,width), posInCoords(0,height,width));
		Debug.DrawLine( posInCoords(0,0,width), posInCoords(width,0,width));
		Debug.DrawLine( posInCoords(0,height,0), posInCoords(0,height,width));
	}
	
}
