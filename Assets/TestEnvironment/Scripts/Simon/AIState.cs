using UnityEngine;
using System.Collections;

public interface AIState {

	// Use this for initialization
	void update(AIBrain brain);
}
