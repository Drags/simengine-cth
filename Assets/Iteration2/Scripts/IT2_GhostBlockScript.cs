using UnityEngine;
using System.Collections;

public class IT2_GhostBlockScript : MonoBehaviour {

	public float transparency = 0.5f;
	
	public Color validColor = new Color(0,1,0, 0.5f);
	public Color invalidColor = new Color(1,0,0, 0.5f);
	
	private bool validBuild;
	
	// Use this for initialization
	void Start () {
		
		//Set properties
		rigidbody.isKinematic = true;
		collider.isTrigger = true;
		
		//Shrink collider 
		BoxCollider col = (BoxCollider)collider;
		col.size = new Vector3(0.85f, 0.85f, 0.85f);
		
		//Set trigger and shrink collider for all children
		foreach (Collider c in GetComponentsInChildren(typeof(Collider) ) ){
			c.isTrigger = true;
			col = (BoxCollider)c;
			col.size = new Vector3(0.85f, 0.85f, 0.85f);
			
		}
		//Set transparency and color
		renderer.material.shader = Shader.Find("Transparent/Diffuse");
		renderer.material.SetColor("_Color", validColor);
		
		//Set transparency and color for all children
		foreach (Renderer r in GetComponentsInChildren(typeof(Renderer) ) ){
			r.material.shader = Shader.Find("Transparent/Diffuse");
			r.material.SetColor("_Color", validColor);
		}
		validBuild = true;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public bool validBuildingPlacement(){
		return validBuild;
	}
	

	void OnTriggerStay(Collider obj){

		renderer.material.SetColor("_Color", invalidColor);
		
		foreach (Renderer r in GetComponentsInChildren(typeof(Renderer) ) )
			r.material.SetColor("_Color", invalidColor);
		validBuild = false;
	}

	void OnTriggerEnter(Collider obj){
		
		renderer.material.SetColor("_Color", invalidColor);
		
		foreach (Renderer r in GetComponentsInChildren(typeof(Renderer) ) )
			r.material.SetColor("_Color", invalidColor);
		validBuild = false;
	}
	
	void OnTriggerExit(Collider obj){
		renderer.material.SetColor("_Color", validColor);
		
		foreach (Renderer r in GetComponentsInChildren(typeof(Renderer) ) )
			r.material.SetColor("_Color", validColor);
		validBuild = true;
	}
}
