using UnityEngine;
using System.Collections;

public interface IT2_iCannonShoot  {

	void shoot(float power);
	void pitch(float angle);
	void turn(float angle);
	
}
