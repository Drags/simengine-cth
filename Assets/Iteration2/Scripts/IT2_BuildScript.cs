using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IT2_BuildScript : MonoBehaviour {

	//The basic building block
	public GameObject bbb;
	
	public List<GameObject> blockList;
	public GameObject selectedBlock;
	
	//The size of one side
	private float bbbSize;
	private float stepSize;
	
	//The ghost objects
	private GameObject ghost;
	
	
	//The starting position of the grid.
	private Vector3 origin;
	//The current position in the grid
	private Vector3 currPos;
	
	public int width = 30;
	public int height = 30;
	
	private IT2_Player player;
	
	
	void Start () {
		
		drawGhost();
		origin = transform.position; 
		bbbSize = ghost.collider.bounds.size.x;
		origin.y += bbbSize/2;
		currPos = new Vector3(0,0,0);
		
		//make the grid cells smaller
		stepSize = bbbSize / 4;
		
		//Set bbb to be selected object
		selectedBlock = bbb;
	}
	
	
	void Update () {
		if (this.player.amOwner()) {
			drawGhost();
			drawGridBounds ();
		}		
	}
	
	public void moveX(int steps){
		//Don't move outside the grid
		if(currPos.x+steps >= 0 && currPos.x+steps <= width)
			currPos.x +=steps;
	}
	
	public void moveY(int steps){
		if(currPos.y+steps >= 0 && currPos.y+steps <= height)
			currPos.y +=steps;
	}
	
	public void moveZ(int steps){
		if(currPos.z+steps >= 0 && currPos.z+steps <= width)
			currPos.z +=steps;
	}
	
	
	//The old one
	private void selectBlock(int block){
		
		if(blockList[block] != null)
			selectedBlock = blockList[block];
		
		//destroy ghost and set ghost to null so a new ghost is generated
		GameObject.Destroy(ghost);
		ghost = null;
	}
	
	public void selectBlock(GameObject block){
		selectedBlock = block;
		GameObject.Destroy(ghost);
		ghost = null;
	}
	
	
	/* Spawns a block
	*  TODO Be able to place different objects, check if valid position, place rotated objects.
	*/
	public GameObject placeBlock(){
		IT2_GhostBlockScript ghostScript = (IT2_GhostBlockScript)ghost.GetComponent("IT2_GhostBlockScript");
		
		if(ghostScript.validBuildingPlacement()){
			GameObject placedBlock = (GameObject)Network.Instantiate(selectedBlock, currentPosInCoords(), selectedBlock.transform.rotation,0);
			return placedBlock;
		}else{
			//Not a valid buildingspot
			return null;
		}
			
	}
	
	/* Spawns a specific block
	*  TODO Be able to place different objects, check if valid position, place rotated objects.
	*/
	public GameObject placeBlock(GameObject block){
		return (GameObject)Network.Instantiate(block, currentPosInCoords(), selectedBlock.transform.rotation,0);
	}
	
	public void setPlayer(IT2_Player player) {
		this.player = player;
	}
	
	private void drawGhost(){
		
		//Move the ghost if there is one
		if(ghost != null){
			ghost.transform.position = currentPosInCoords();
		}else{
			//Create new ghost from selected block
			ghost = (GameObject)Instantiate(selectedBlock, currentPosInCoords(), selectedBlock.transform.rotation);
			ghost.AddComponent("IT2_GhostBlockScript");
		}
		
	}
	
	public void rotateY(float r){
		selectedBlock.transform.Rotate(0,r,0);
		ghost.transform.Rotate(0,r,0);
	}
	
	public Vector3 currentPosInCoords(){
		
		
		Vector3 pos = transform.position;
		
		//Move to origin
		pos.x = origin.x;
		pos.y = origin.y;
		pos.z = origin.z;
		
		//Move to the currentposition in the grid
		pos.x += currPos.x*stepSize;
		pos.y += currPos.y*stepSize;
		pos.z += currPos.z*stepSize;
		
		return pos;
		
	}
	
	private Vector3 posInCoords(int x, int y, int z){
	
		Vector3 p = transform.position;
		
		//Move to origin
		p.x = origin.x;
		p.y = origin.y;
		p.z = origin.z;
		
		//Move to the currentposition in the grid
		p.x += x*stepSize;
		p.y += y*stepSize;
		p.z += z*stepSize;
		
		return p;
		
		
	}
	
	public Vector3 getPosInGrid(){
		Vector3 returnPositions;
		returnPositions.x = currPos.x;
		returnPositions.y = currPos.y;
		returnPositions.z = currPos.z;
		return returnPositions;
		
	}
	
	public Vector3 getGridOrigin(){
		return origin;
	}
	
	public Vector3 getRealCoordsToGrid(Vector3 coords){
		Vector3 returnCoords = coords - origin;
		returnCoords.x = returnCoords.x*(1/stepSize);
		returnCoords.y = returnCoords.y*(1/stepSize);
		returnCoords.z = returnCoords.z*(1/stepSize);
		return returnCoords;
		
		
	}
	
	/*
	 * Draw some lines to visualize the bounds of the buildinggrid
	 * 
	 */
	private void drawGridBounds(){
		
		Debug.DrawLine( posInCoords(0,0,0), posInCoords(width,0,0));
		Debug.DrawLine( posInCoords(0,0,0), posInCoords(0,height,0)); 
		Debug.DrawLine( posInCoords(0,0,0), posInCoords(0,0,width)); 
		
		Debug.DrawLine( posInCoords(width,height,width), posInCoords(width,height,0));
		Debug.DrawLine( posInCoords(width,height,width), posInCoords(width,0,width)); 
		Debug.DrawLine( posInCoords(width,height,width), posInCoords(0,height,width));
		
		Debug.DrawLine( posInCoords(width,height,0), posInCoords(0,height,0));
		Debug.DrawLine( posInCoords(width,height,0), posInCoords(width,0,0));
		Debug.DrawLine( posInCoords(width,0,width), posInCoords(width,0,0));
		
		Debug.DrawLine( posInCoords(0,0,width), posInCoords(0,height,width));
		Debug.DrawLine( posInCoords(0,0,width), posInCoords(width,0,width));
		Debug.DrawLine( posInCoords(0,height,0), posInCoords(0,height,width));
	}
}
