using UnityEngine;
using System.Collections;

public class IT2_Heart : MonoBehaviour, IT2_iHeart {

	private int playerNumber;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	[RPC]
	public void setPlayerNumber(int player) {
		this.playerNumber = player;
	}
	
	public int getPlayerNumber() {
		return this.playerNumber;
	}	
	
	public float getHealth(){
		return GetComponent<IT2_DamageReceiver>().health ;
	}
}
