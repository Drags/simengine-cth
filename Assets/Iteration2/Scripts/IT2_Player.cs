using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class IT2_Player : MonoBehaviour {
	
	public int numberTest;
	
	//Cannon
	public IT2_iCannonShoot iCannon; //Interface to turn, pitch and shoot the cannon	
	public GameObject pCannon;
	public GameObject currentCannon;
	
	//Heart
	public IT2_iHeart iHeart; 
	public GameObject pHeart;
	public GameObject currentHeart;
	
	//Blocks
	public List<GameObject> blockTypes;
	public List<KeyValuePair<GameObject,int>> blocksInventory; //used keyvaluepair because Tuple isn't in this version of C#
	public List<GameObject> blocksPlaced;
	
	//Building Area	
	public GameObject pBuildingArea;
	public GameObject currentBuildingArea;
	public IT2_BuildScript buildScript;
	
	public NetworkPlayer owner;
	
	//Cooldown for moving through the grid. The player doesn't have any precision without it
	private float buildMoveCD = 0;
	
	
	// Use this for initialization
	IEnumerator Start () {
	
		blocksPlaced = new List<GameObject>();
		blocksInventory = new List<KeyValuePair<GameObject, int>>();
		//Create inventory from blocktypes
		//Basic cube
		blocksInventory.Add(new KeyValuePair<GameObject,int>(blockTypes[0],20));
		//Long block
		blocksInventory.Add(new KeyValuePair<GameObject,int>(blockTypes[1],5));
		
		
		createBuildArea();
		yield return null;

		
		createHeart();
		
		createCannon();
		
		yield break;
		
		
	}
	
	// Update is called once per frame
	void Update () {
		
		cleanList(blocksPlaced);
		readControls();
		relocatePlayerChilds();
		
		//Subtract from the cooldown
		buildMoveCD -= 0.5f;
		if(buildMoveCD<0)
			buildMoveCD = 0;

	
	}
	
	// additional Unity Networking API workaround, to account for the fact that sometimes RPC and Network.Instantiate are not sequential...
	private void relocatePlayerChilds() {
		if (currentCannon == null) {
			GameObject[] allCannons = GameObject.FindGameObjectsWithTag("CannonPF");
			foreach(GameObject cannon in allCannons) {
				IT2_CannonShoot cannonScript = cannon.GetComponent<IT2_CannonShoot>();
				if (cannonScript.getPlayerNumber() == getPlayerNumber()) {
					currentCannon = cannon;
				}
			}
			currentCannon.GetComponent<IT2_CannonShoot>().setPlayer(this);
			iCannon = currentCannon.GetComponent<IT2_CannonShoot>();
		}
	}
	
	public void ResetPlayer() {
		foreach (GameObject obj in blocksPlaced) {
			Destroy(obj);
		}
		foreach (KeyValuePair<GameObject,int> obj in blocksInventory) {
			Destroy(obj.Key);
		}
		blocksPlaced.Clear();
		blocksInventory.Clear();
		
	}

	[RPC]
	public void setPosition(Vector3 position) {
		transform.position = position;
	}
	
	[RPC]
	public void setOwner(NetworkPlayer newOwner) {
		this.owner = newOwner;
		currentCannon.GetComponent<IT2_CannonShoot>().setPlayerNumber(getPlayerNumber());
		currentHeart.GetComponent<IT2_Heart>().setPlayerNumber(getPlayerNumber());
	}
	
	public bool isAI() {
		return this.GetComponent<IT2_AIBrain>().isAwake();
	}
	
	public bool amOwner() {
		return (this.owner != null && this.owner == Network.player);
	}
	
	public int getPlayerNumber() {
		if (isAI()) return -1;
		return Convert.ToInt32(this.owner+"");
	}
	
	private void readControls(){
		
		if(!(GetComponent<IT2_AIBrain>().isAwake())){
			//Only do this if in building phase.
			if (IT2_InputSelector.activeInput == IT2_InputSelector.activeControl.building)
			{
				if (this.amOwner()){
					//Keys for debugging
					if(Input.GetKey(KeyCode.W) && buildMoveCD == 0 )
						buildScript.moveX(1);
					if(Input.GetKey(KeyCode.S) && buildMoveCD == 0 )
						buildScript.moveX(-1);
					if(Input.GetKey(KeyCode.Q) && buildMoveCD == 0 )
						buildScript.moveY(-1);
					if(Input.GetKey(KeyCode.E) && buildMoveCD == 0 )
						buildScript.moveY(1);
					if(Input.GetKey(KeyCode.A) && buildMoveCD == 0 )
						buildScript.moveZ(1);
					if(Input.GetKey(KeyCode.D) && buildMoveCD == 0 )
						buildScript.moveZ(-1);
					if(Input.GetKeyDown(KeyCode.Z))
						buildScript.selectBlock(blocksInventory[0].Key);
					if(Input.GetKeyDown(KeyCode.X))
						buildScript.selectBlock(blocksInventory[1].Key);
					if(Input.GetKeyDown(KeyCode.R))
						buildScript.rotateY(90);
					if(Input.GetKeyDown(KeyCode.Space)){
						GameObject blockPlaced = buildScript.placeBlock();
						if(blockPlaced != null){
							blocksPlaced.Add(blockPlaced);
							foreach(KeyValuePair<GameObject,int> pair in blocksInventory){
								if(pair.Key == buildScript.selectedBlock){
									KeyValuePair<GameObject,int> temp =  new KeyValuePair<GameObject,int>(buildScript.selectedBlock, pair.Value-1);
									int index = blocksInventory.IndexOf(pair);
									blocksInventory[index] = temp;
								}
							}
						}	
					}
					//add to the move cooldown if a move key is being pushed
					if((Input.GetKey(KeyCode.W)||Input.GetKey(KeyCode.S)||Input.GetKey(KeyCode.Q)
						||Input.GetKey(KeyCode.E)||Input.GetKey(KeyCode.A)||Input.GetKey(KeyCode.D)) && buildMoveCD == 0){
						buildMoveCD += 2;
					}
				}
			}
			
			if(IT2_InputSelector.activeInput == IT2_InputSelector.activeControl.canon)
			{
									//DEBUG KEYS
				if(Input.GetKey(KeyCode.RightArrow))
					iCannon.turn(IT2_CannonShoot.turnSpeed);
				if(Input.GetKey(KeyCode.LeftArrow))
					iCannon.turn(-IT2_CannonShoot.turnSpeed);
				if(Input.GetKey(KeyCode.UpArrow))
					iCannon.pitch(-IT2_CannonShoot.pitchSpeed);
				if(Input.GetKey(KeyCode.DownArrow))
					iCannon.pitch(IT2_CannonShoot.pitchSpeed);
			
				if(Input.GetKeyDown(KeyCode.Space)){
					iCannon.shoot(30);
				}
			
				
			}
		}
	}
	
	private void createBuildArea(){
			currentBuildingArea = (GameObject)Instantiate(pBuildingArea, transform.position, transform.rotation);
			buildScript = currentBuildingArea.GetComponent<IT2_BuildScript>();
			buildScript.setPlayer(this);
	}
	
	private void createHeart(){
		buildScript.moveX(buildScript.width/2);
		buildScript.moveZ (buildScript.width/2);
		// workaround of Unity Networking API (when it doesn't allow to transmit GameObject over RPC
		if (Network.isServer) {
			currentHeart = buildScript.placeBlock(pHeart);
			currentHeart.GetComponent<IT2_Heart>().setPlayerNumber(getPlayerNumber());
			currentHeart.networkView.RPC("setPlayerNumber", RPCMode.AllBuffered, getPlayerNumber());
		} else if (Network.isClient) {
			// just selecting heart which belongs to local player
			GameObject[] allHearts = GameObject.FindGameObjectsWithTag("TBox");
			foreach(GameObject heart in allHearts) {
				IT2_Heart heartScript = heart.GetComponent<IT2_Heart>();
				if (heartScript.getPlayerNumber() == getPlayerNumber() || heartScript.getPlayerNumber() == -1) {
					currentHeart = heart;
					heartScript.setPlayerNumber(getPlayerNumber());
					break;
				}
			}
		}
		iHeart = currentHeart.GetComponent<IT2_Heart>();
		
		buildScript.moveZ (-buildScript.width/2);
		buildScript.moveX (-buildScript.width/2);
	}
	
	private void createCannon(){
		Vector3 cannonTarget;
		buildScript.moveX (buildScript.width/2);
		buildScript.moveZ (buildScript.width);
		
		cannonTarget = buildScript.currentPosInCoords();
		cannonTarget.z += 5;
		
		// workaround of Unity Networking API (when it doesn't allow to transmit GameObject over RPC
		if (Network.isServer) {
			currentCannon = (GameObject) Network.Instantiate(pCannon, cannonTarget, transform.rotation, 0);
			currentCannon.networkView.RPC("setPlayerNumber", RPCMode.AllBuffered, getPlayerNumber());
			currentCannon.GetComponent<IT2_CannonShoot>().setPlayerNumber(getPlayerNumber());
		} else {
			// just selecting cannon which belongs to local player
			GameObject[] allCannons = GameObject.FindGameObjectsWithTag("CannonPF");
			foreach(GameObject cannon in allCannons) {
				IT2_CannonShoot cannonScript = cannon.GetComponent<IT2_CannonShoot>();
				if (cannonScript.getPlayerNumber() == getPlayerNumber() || cannonScript.getPlayerNumber() == -1) {
					currentCannon = cannon;
					cannonScript.setPlayerNumber(getPlayerNumber());
					break;
				}
			}
		}
		
		currentCannon.GetComponent<IT2_CannonShoot>().setPlayer(this);
		iCannon = currentCannon.GetComponent<IT2_CannonShoot>();
	}
	
	
	[RPC]
	public void executePitch(float angle) {
		currentCannon.GetComponent<IT2_CannonShoot>().executePitch(angle);
	}
	
	[RPC]
	public void executeTurn(float angle) {
		currentCannon.GetComponent<IT2_CannonShoot>().executeTurn(angle);
	}
	
	[RPC]
	public void executeShoot(float power) {
		currentCannon.GetComponent<IT2_CannonShoot>().executeShoot(power);
	}
	
	public void removeBlockFromScene(GameObject toRemove){
		
		blocksPlaced.Remove(toRemove);
	}
	
	private void cleanList(List<GameObject> dirtyList){
		dirtyList.RemoveAll(
			delegate(GameObject go){
				return go == null;
			}
		);
	}
}