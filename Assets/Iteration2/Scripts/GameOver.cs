using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour {
	int width = 150;	//Size of GUI rectangle
	string msg;				//Message to display in GUI rectangle
	
	//Dummy values
	public bool gameOver = false;
	public bool playerWon = false;
	public int height;
	
	GUIStyle guiStyle;
	
	//Screen width and height
	int sWidth;
	int sHeight;
	
	//variables for the button's appearance and position
	int buttonXPos;
	int buttonWidth;
	int buttonYPos;
	int buttonHeight;
	
	// Use this for initialization
	void Start () {
		guiStyle = new GUIStyle();
		guiStyle.fontSize = 20;
		
		buttonWidth = 100; 
		buttonHeight = 40; 
		height = 100;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	//Displays the game over menu & message
	 void OnGUI() {
		//Read gameOver & playerWon vars
		if(gameOver){
			sWidth = Screen.width;
			sHeight = Screen.height;
			//decide who won
        	if(playerWon){
				msg = "You Won!";
			}else {
				msg = "You Lost";
			}
			//Show game over screen
			GUI.Box(new Rect((sWidth/2)-width, (sHeight/2)-width/2, width*2, height), msg);
			
			//calculate new game button position and size
			int buttonXPos = sWidth/2 - buttonWidth/2;
			int buttonYPos = sHeight/2 + height/2- buttonHeight - 40; //place button 10px above bottom of box 
			//Display and listen to button
			//Shift this button buttonwidth +10px to the left to accomondate button nr 2
			if(GUI.Button(new Rect(buttonXPos-buttonWidth/2 - 10, buttonYPos, buttonWidth, buttonHeight), "Main Menu")){
				//Bring up menu
				KeyboardListener keyboardListener = GameObject.Find ("Scene").GetComponent<KeyboardListener>();
				keyboardListener.gameStarted = true;
				keyboardListener.UpdateMenu();
			}
			if(GUI.Button(new Rect(buttonXPos+buttonWidth/2 + 10, buttonYPos, buttonWidth, buttonHeight), "Exit Game")){
				Application.Quit();
			}
		}
    }
}
