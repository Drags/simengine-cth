using UnityEngine;
using System.Collections;

public class MenuPicking : MonoBehaviour {
	public Component[] listOfChildren;
	public static float TEXT_NUDGE = 0.05f; //how far the menu items will be nudged on mouseover
	public static float LIGHTROTATION = 3f; //how fast the light will rotate on startup
	string lastPicked = ""; //keeps track of the last picked menu item

	
	// Use this for initialization
	void Start () {
				

    }
		
	
	/*Detects which menu item was picked*/
	void Update () {

		SkipMenuCheck();
		
		RotateLight(); //rotates light on startup
		
		//Store the object that is clicked on or "mouse-overed"
		GameObject pickedObject = GetObjectFromRay ();
		
		/*Find which object was clicked on*/
		
		//On mouseClick
		if (Input.GetMouseButtonDown (0)) {	
			if (pickedObject != null) {
				
				//Determine which menu item was clicked on
				//New Game button pressed
				if(pickedObject.name == "NewGameBB"){
					//Play click sound
					GameObject.Find("NewGameBB").GetComponent<AudioSource>().audio.Play();
					StartNewGame();
					
				}
				//Exit game button pressed
				if(pickedObject.name == "ExitGameBB"){
					GameObject.Find("ExitGameBB").GetComponent<AudioSource>().audio.Play();
					Application.Quit();
				}
			}
		}
		//On mouseOver (no mouse button down)
		else{
			if (pickedObject != null) {
				//New Game
				if(pickedObject.name.Equals("NewGameBB")){
					//Give feedback if this button is not already hovered over
					if(!(lastPicked.Equals ("NewGameBB"))){
						pickedObject.transform.parent.Translate(0,0,TEXT_NUDGE);
						lastPicked = "NewGameBB";
					}
				}
				else if(pickedObject.name.Equals("ExitGameBB")){
					//Give feedback if this button is not already hovered over
					if(!(lastPicked.Equals ("ExitGameBB"))){
						pickedObject.transform.parent.Translate(0,0,TEXT_NUDGE);
						lastPicked = "ExitGameBB";
					}
				}
				//Other
				else if(lastPicked.Equals("NewGameBB")){
					//undo feedback on mouseout
					lastPicked = ""; // only move text back once by setting the lastPicked to "" to not move back again
					GameObject.FindWithTag("NewTextParent").transform.Translate(0,0,-TEXT_NUDGE);
				}
				else if(lastPicked.Equals("ExitGameBB")){
					//undo feedback on mouseout
					lastPicked = "";
					GameObject.Find("ExitGame3D").transform.Translate(0,0,-TEXT_NUDGE);
				}
				

			}
				
		}
	}
	
	/*Returns the tag of the picked object
	Modified "getBodyFromRay" in JohanJointTest.cs*/
	GameObject GetObjectFromRay ()
	{
		Ray r = Camera.main.ScreenPointToRay (Input.mousePosition);
		RaycastHit rHit;
		
		if (Physics.Raycast (r, out rHit)) {
			return rHit.collider.gameObject;
		}
		
		return null;
	}
	
	//Rotates the light to the menu plane on startup
	void RotateLight(){
		float time = Time.timeSinceLevelLoad; //tim in seconds since level loaded
		float rotation = GameObject.Find ("DirectionalLight").transform.eulerAngles.x; //get light rotation
		//rotate light untill it is perpendicular to menu plane
		if( rotation < 359 && rotation >= 270 )
				GameObject.Find("DirectionalLight").transform.Rotate( Hermite(0,2,time)*LIGHTROTATION,0,0 );
		
	}
	
	//Returns the hermite (ease in & out) interpolation of var "value"
	public static float Hermite(float start, float end, float value)
    {
        return Mathf.Lerp(start, end, value * value * (3.0f - 2.0f * value));
    }
    
	//Skips displaying the menu
	private void SkipMenuCheck(){
		//gameStarted object is persisted when resetting the scene, and so is used for checking if 
		//the menu should be skipped, since if it exists the user pressed the new game button in the menu
		//whereby the scene was re-loaded,
		if(GameObject.Find("gameStarted") != null){
			HideMenu ();
			//GameObject.Find("Spawner").GetComponent<IT2_GameManager>().NewGame();
			Destroy(GameObject.Find("gameStarted"));
		}
	}
	//Starts a new game
	private void StartNewGame(){
		HideMenu();
		GameObject.Find("Spawner").GetComponent<IT2_GameManager>().NewGame();		
	}
	
	//Hides the menu
	private void HideMenu(){
		//PETRAS: enabling networking here, so that networking and player spawning happens only when New game pressed
			GameObject.Find("Spawner").GetComponent<IT2_SeamlessNetworkPlay>().StartNetworking();
			
			//Start game, switch camera
			KeyboardListener keyboardListener = GameObject.Find ("Scene").GetComponent<KeyboardListener>();
			keyboardListener.gameStarted = true;
			keyboardListener.UpdateMenu();
			
	}
}

