#pragma strict
var constantVelocity : Vector3 = Vector3(0,0,0);
var oneDirectionX : boolean = false;
var oneDirectionY : boolean = false;
var oneDirectionZ : boolean = false;
var oppositeDirectionX : boolean = false;
var oppositeDirectionY : boolean = false;
var oppositeDirectionZ : boolean = false;
//var changeVelocity : long = 0.0;
function Start () {
	if(oneDirectionX){
		constantVelocity = Vector3(Mathf.Abs(constantVelocity.x), constantVelocity.y, constantVelocity.z);
		if(oppositeDirectionX){
			constantVelocity = Vector3(-constantVelocity.x, constantVelocity.y, constantVelocity.z);
		}
	}
	if(oneDirectionY){
		constantVelocity = Vector3(constantVelocity.x, Mathf.Abs(constantVelocity.y), constantVelocity.z);
	}
		if(oppositeDirectionY){
			constantVelocity = Vector3(constantVelocity.x, -constantVelocity.y, constantVelocity.z);
		}
	if(oneDirectionZ){
		constantVelocity = Vector3(constantVelocity.x, constantVelocity.y, Mathf.Abs(constantVelocity.z));
		if(oppositeDirectionZ){
			constantVelocity = Vector3(constantVelocity.x, constantVelocity.y, -constantVelocity.z);
		}
	}
}

function Update () {
	//constantVelocity = constantVelocity + Vector3(changeVelocity, changeVelocity, changeVelocity);
	transform.position += constantVelocity * Time.deltaTime;
}