using UnityEngine;
using System;
using System.Collections;

public class IT2_GameManager : MonoBehaviour {
	
	//The length of the buildphase in the beginning 
	private int initialBuildPhaseLength = 100;

	public Transform playerPrefab;
	static public ArrayList playerScripts;
	
	private Vector3[] spawnpoints;
	private int spawnedplayers;

	IEnumerator OnServerInitialized(){
		//Spawn a player for the server itself
		Spawnplayer(Network.player,spawnpoints[spawnedplayers], false);
		//Spawn an AI player
		Spawnplayer(Network.player,spawnpoints[spawnedplayers], true);
		yield return new WaitForSeconds(0.1f);;
		
		//set cooldown for each cannon at game start
		foreach(IT2_Player player in playerScripts){
			IT2_CannonShoot shootScript = (IT2_CannonShoot)player.currentCannon.GetComponent("IT2_CannonShoot");
			shootScript.setCooldown(initialBuildPhaseLength);
		}
	}
	

	void OnPlayerConnected(NetworkPlayer newPlayer) {
		//A player connected to me(the server)!
		foreach(IT2_Player player in playerScripts) {
			IT2_AIBrain aiScript = player.GetComponent<IT2_AIBrain>();
			if (aiScript.isAwake()) {  // controlled by AI, lets replace
				aiScript.StopAI();
				int playerNumber = Convert.ToInt32(newPlayer+"");
				player.networkView.group = playerNumber;
				player.networkView.RPC("setOwner", RPCMode.AllBuffered, newPlayer); //Set it on the owner
				player.GetComponent<IT2_Player>().setOwner(newPlayer);
				return;
			}
		}
		// No AI player, spawn new one
		Spawnplayer(newPlayer,spawnpoints[spawnedplayers], false);
	}	

	static public IT2_Player getLocalPlayer() {
	GameObject[] allPlayers = GameObject.FindGameObjectsWithTag("PlayerObjectPF");
		foreach(GameObject player in allPlayers) {
			IT2_Player potentialScript = player.GetComponent<IT2_Player>();
			if (potentialScript.amOwner()) {
				return potentialScript;
			}
		}
		return null;
	}
	
	static public IT2_Player getEnemyPlayer() {
		GameObject[] allPlayers = GameObject.FindGameObjectsWithTag("PlayerObjectPF");
		foreach(GameObject player in allPlayers) {
			IT2_Player potentialScript = player.GetComponent<IT2_Player>();
			if (!potentialScript.amOwner() || potentialScript.isAI()) {
				return potentialScript;
			}
		}
		return null;
	}
	
	void OnPlayerDisconnected(NetworkPlayer player) {
		// let's startup AI again, when player disconnects
		foreach(IT2_Player script in playerScripts){
			if(player==script.owner){//We found the players object
				script.GetComponent<IT2_AIBrain>().StartAI();
				break;
			}
		}
	}

	void OnDisconnectedFromServer(NetworkDisconnection info) {
		Application.LoadLevel(Application.loadedLevel);	
	}
	
	void Spawnplayer(NetworkPlayer newPlayer, Vector3 position, bool AIControlled){
		spawnedplayers = (spawnedplayers + 1) % spawnpoints.Length;
		//Called on the server only
		
		int playerNumber = Convert.ToInt32(newPlayer+"");
		//Instantiate a new object for this player, remember; the server is therefore the owner.
		Transform myNewTrans = (Transform)Network.Instantiate(playerPrefab, position, transform.rotation, 0);
		
		//Get the networkview of this new transform
		NetworkView newObjectsNetworkview = myNewTrans.networkView;
		
		//Keep track of this new player so we can properly destroy it when required.
		playerScripts.Add(myNewTrans.GetComponent<IT2_Player>());
		
		if (AIControlled) {
			myNewTrans.GetComponent<IT2_AIBrain>().StartAI();
		} else {
			//Call an RPC on this new networkview, set the player who controls this player
			myNewTrans.GetComponent<IT2_Player>().setOwner(newPlayer);
			newObjectsNetworkview.RPC("setOwner", RPCMode.AllBuffered, newPlayer);//Set it on the owner
		}
	}
	
	//Starts a new game
	public void NewGame(){
		//If game has not been started before, create flag object to indicate that game has been started now
		if(GameObject.Find("gameStarted") == null){
			//Used as flag between scene resets to indicate that the game has been started this session
			GameObject gameStarted = new GameObject("gameStarted"); 
			//Persist flag object when reloading scene
			DontDestroyOnLoad(gameStarted);
		}
		Application.LoadLevel("GameScene");
		Time.timeScale = 1.0f;
	}
	

	// Use this for initialization
	void Start () {
		playerScripts = new ArrayList();
		int areasize = 10;
		spawnpoints = new Vector3[]{
			new Vector3(3.36f,25.50556f,60.87f),
			new Vector3(61.4f,25.50556f,60.87f)
		};
		spawnedplayers = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
		if(getLocalPlayer().currentHeart == null){
			Time.timeScale = 0.2f;
			GameObject.Find("Main Camera").GetComponent<GameOver>().playerWon = false;
			GameObject.Find("Main Camera").GetComponent<GameOver>().gameOver = true;
		}
		if(getEnemyPlayer().currentHeart == null){
			Time.timeScale = 0.2f;
			GameObject.Find("Main Camera").GetComponent<GameOver>().playerWon = true;
			GameObject.Find("Main Camera").GetComponent<GameOver>().gameOver = true;
		}
		
		
	}
}
