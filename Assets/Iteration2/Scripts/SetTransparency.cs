using UnityEngine;
using System.Collections;

public class SetTransparency : MonoBehaviour {
	public float transparency = 0.5f;
	// Use this for initialization
	void Start () {
		gameObject.renderer.material.shader = Shader.Find ("Transparent/Diffuse");
		Color a = gameObject.renderer.material.GetColor("_Color");
		a.a = transparency;
		gameObject.renderer.material.SetColor("_Color",a);
	}
	
	// Update is called once per frame
	void Update () {
	}
}
