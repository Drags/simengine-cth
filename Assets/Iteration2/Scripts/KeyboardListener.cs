using UnityEngine;
using System.Collections;

public class KeyboardListener : MonoBehaviour {
	bool menuActive = true; //Keep track of whether the menu object is active or not
	public bool gameStarted = false;
	//reference to the parent menu object, needed since GameObject.Find("Menu") returns null when object is inactive
	GameObject menu;
	GameObject mainCamera; //reference to the main camera used outside of the menu
	// Use this for initialization
	void Start () {
		menu = GameObject.Find("Menu");
		mainCamera = GameObject.Find("Main Camera");
		if(mainCamera == null){print("MAIN CAM NULL");}
		mainCamera.SetActive(false); //start game using the menu camera
	}
	
	// Update is called once per frame
	void Update () {

		//Hide/show menu
		if(Input.GetKeyDown(KeyCode.Escape)){
			UpdateMenu();
		}

	}
	
	//enables/disables the main menu
	public void UpdateMenu(){
		//if game is paused, resume
		if(menuActive == true && gameStarted){
			//GameObject.Find("MenuCamera").SetActive(false);
			menu.SetActive(false);
			menuActive = false;
			mainCamera.SetActive(true);
			
		}
		//if in-game, bring up menu
		else if(menuActive == false){
			menu.SetActive(true);
			menuActive = true;
			mainCamera.SetActive(false);
		}
	}
}
