using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/*
 * The visualization for player health, enemy health and player number of blocks remaining 
 */ 
public class EntityViz : MonoBehaviour {
	
	//Textures on each vertex
	public Texture[] icons;		//Player health, Enemy health, Blocks left textures
	
	private LineRenderer[] lr;	//stores the line renderer objects used for the polygon
	private static float lineTransparency = 0.03f; // std. line transparency
	
	//Variables used in linerenderer 
	Rect pixelRect;			//The rectangle that the camera renders
	Camera cam;				//reference to the menuCamera's camera object
	float nearClipPlane;	//the nearclip plane, to which the visualization should be rendered
	
	//Gameplay variables
	public int health;							//player health
	private static int MAX_HEALTH = 25; 		//max player health
	
	public int eHealth;						//enemy player health
	private static int MAX_ENEMY_HEALTH = 25; 	//max player health
	
	public int blocks;							//player blocks left
	private static int MAX_BLOCKS = 25; 		//max player blocks
	
	private int[] values; 		// array to hold attribute variables
	private int[] maxValues; 	// array to hold maximum values for attributes
	
	//Used to identify each attribute in an array
	private static int EHEALTH = 0;
	private static int HEALTH = 1;
	private static int BLOCKS = 2;
	
	private static int NRATT = 3; //The number of attributes to be visualized
	
	//Vertex positions in the triangle
	Vector3[] v; 		
	
	//Variables determining the vertex positions on startup
	float v0dx; 		//Vertex 0 delta x (offset in pixels from edge of screen)
	float v0dy;
	float v1dx; 		//Vertex 1 delta x (offset in pixels from vertex 0)
	float v1dy;
	float v2dx;			//Vertex 2 delta x (will be offset from vertex 0 by 2* vertex one offset in a equilateral triangle)
	float v2dy;
	
	//Effective visualization values (the values to be rendered)
	float healthComponent;		//player health	
	float blocksComponentX;		//player blocks left
	float blocksComponentY;		//player blocks left
	float eHealthComponentX;	//enemy player health left
	float eHealthComponentY;	//enemy player health left
	
	Vector3 center; //center of triangle
	
	// Use this for initialization
	void Start () {
		CreateEntityViz();
	}
	
	// Update is called once per frame
	void Update () {
		UpdateEntityViz();
	}
	
	//Set up the entityViz (Unity linerenderer is used)
	void CreateEntityViz(){	
		
		v = new Vector3[NRATT+1]; // init array of vertex positions 
		
		//init health & block dummy values
		health = MAX_HEALTH;
		blocks = MAX_BLOCKS;
		eHealth = MAX_ENEMY_HEALTH;
		
		values = new int[NRATT+1]; // holds attribute values to be visualized
		
		//Read maximum values of attributes
		maxValues = new int[NRATT+1];
		maxValues[EHEALTH] = MAX_ENEMY_HEALTH;
		maxValues[HEALTH] = MAX_HEALTH;
		maxValues[BLOCKS] = MAX_BLOCKS;
		maxValues[NRATT] = maxValues[EHEALTH]; //Last vertex = first
		
		//Variables used in linerenderer 
		cam = gameObject.camera;
		pixelRect = cam.pixelRect;
		nearClipPlane = camera.nearClipPlane + 0.1f;	//offset triangle from nearclip plane slightly
		
		//Variables determining the vertex positions on startup
		v0dx =	20; 		//Indent vertex 0 (=> the triangle) this much from edge of screen
		v0dy =	v0dx;	
		v1dx =	0.08f * pixelRect.xMax;	//width of triangle = x*screenwidth
		//The height of v1 (v1dy) must be calculated according to the formula height = tan(45)*adjacent(v1dx)
		//to end up with an equilateral triangle
		v1dy = 	(v1dx * Mathf.Tan( ((60*Mathf.PI) / 180) )); 
		v2dx = 	2*v1dx;
		v2dy = 	v0dy;
		
		//Components decide which direction and how much to offset vertex from original position
		//Set all components to default (vertex) values initially ( no offset on startup)
		healthComponent = v1dy; 	//effective offset for vertex 1 (y)
		blocksComponentX = v2dx;	//effective offset for vertex 2 (x)
		blocksComponentY = v2dy;	//effective offset for vertex 2 (y)
		eHealthComponentX = v0dx;	//effective offset for vertex 0 (x)
		eHealthComponentY = v0dy;	//effective offset for vertex 0 (y)
		
		//find center of triangle
		center = new Vector3( ( (v0dx + v1dx + v2dx) / 3 ) , ( (v0dy + v1dy + v2dy) / 3 ) , nearClipPlane);
		
		// Create one linerenderer for each line to be drawn
		// Is necessary to be able to assign each vertex (= each end of the line) a separate color
		// Also eliminates the problem of the lines folding 
		// Create twice as many line renderers as needed for the polyogns to draw shadow polygon
		lr = new LineRenderer[(NRATT*2)+1]; 
		for(int i = 0 ; i < NRATT*2 ; i++){
			lr[i] = new GameObject().AddComponent("LineRenderer") as LineRenderer;
	        lr[i].material = new Material(Shader.Find("Particles/Additive"));
	        lr[i].SetWidth(0.01F, 0.01F);
			//std. line color (will be used for shadow polygon
			lr[i].SetColors( new Color(1f , 1f, 1f, lineTransparency),  new Color(1f , 1f, 1f, lineTransparency));
	        lr[i].SetVertexCount(2);
		}
		
		
        
	}
	
	//Redraw entityViz to reflect changed entity values
	void UpdateEntityViz(){		
		
		//Start and end coordinates for line renderers
		Vector3 screenLoc;
		Vector3 screenLoc1;
		
		DrawShadowTriangle(lr);
		
		/*read dummy attribute values*/
		/*
		values[EHEALTH] = eHealth;
		values[HEALTH] = health;
		values[BLOCKS] = blocks;
		values[NRATT] = values[EHEALTH]; //copy of vertex 1
		*/
		
		IT2_Player player = IT2_GameManager.getLocalPlayer();	//Reference to local player
		IT2_Player enemy = IT2_GameManager.getEnemyPlayer();
		
		/*Read ingame attributes*/
		//Enemy health
		values[EHEALTH] =	(int)enemy.currentHeart.GetComponent<IT2_DamageReceiver>().health;
		//Player Health
		values[HEALTH] =	(int)player.currentHeart.GetComponent<IT2_DamageReceiver>().health;
		//Player blocks left
		//the structure containing the number of blocks left of each type
		List<KeyValuePair<GameObject,int>> blocks = player.blocksInventory;
		values[BLOCKS] = 0;	//reset the number of blocks visualized
		//traverse all types of blocks
		foreach(KeyValuePair<GameObject,int> pair in  blocks){
			//Add the current number of blocks left for each type of block to the total of blocks left
			values[BLOCKS] += pair.Value;
		}
		values[NRATT] =		values[EHEALTH]; //copy of vertex 1, used for iterating through the vertexes, when reaching the last vertex, draw a line to the first vertex
		
		/*Run ingame attribute values through calculation to prepare them for visualization in the polygon*/
		//Map eg. health from (range = 0->MAX_HEALTH) to
		//visualization (range = center of triangle -> vertex1 y start pos)
		//Also clamp vertex positions so that triangle does not grow larger than 
		//the original or vertexes reach past the center of the triangle
		healthComponent = 	Mathf.Clamp(map( 0 , MAX_HEALTH , center.y , v1dy , values[HEALTH]), center.y, v1dy);
		
		blocksComponentX =	Mathf.Clamp(map( 0 , MAX_BLOCKS , center.x , v2dx , values[BLOCKS] ), center.x, v2dx );
		blocksComponentY =	Mathf.Clamp(map( 0 , MAX_BLOCKS , center.y,	 v2dy ,	values[BLOCKS] ), v2dy, center.y);
		
		eHealthComponentX =	Mathf.Clamp(map( 0 , MAX_ENEMY_HEALTH , v0dx ,center.x,	MAX_ENEMY_HEALTH - values[EHEALTH] ), v0dx, center.x );
		eHealthComponentY =	Mathf.Clamp(map( 0 , MAX_ENEMY_HEALTH , v0dy ,center.y,	MAX_ENEMY_HEALTH - values[EHEALTH]), v0dy, center.y);
		
		//Prepare vertex positions for use in rendering
		v[EHEALTH] = new Vector3(eHealthComponentX,	eHealthComponentY,			nearClipPlane);
		v[HEALTH] = new Vector3(v1dx,				healthComponent,		nearClipPlane);
		v[BLOCKS] = new Vector3(blocksComponentX,	blocksComponentY ,			nearClipPlane);
		v[NRATT] = v[EHEALTH]; // last vertex to be rendered must close the polyogn => draw line back to first vertex

		//draw lines from vertex 0 to last vertex and back to vertex 0
		for(int i = 0; i < 3 ; i++){
			
			// transforming screenspace coordinate to worldspace needed for linerenderer
			//as it only renders to worldspace
			screenLoc = cam.ScreenToWorldPoint(v[i]); 
			screenLoc1 = cam.ScreenToWorldPoint(v[i+1]); 
			
			float startVertexValue = ( (float)values[i] / maxValues[i] );	//used to calculate the color of vertex at beginning of line
			float endVertexValue = ( (float)values[i+1] / maxValues[i+1] );	//used to calculate the color of vertex at end of line
			
			//The colors to be rendered at the beginning and end of the line
			Color c1 = new Color( 1.0f - startVertexValue ,0f, startVertexValue/*,1.2f-startVertexValue */);
			Color c2 = new Color( 1.0f - endVertexValue, 0f, endVertexValue/*, 1.2f-startVertexValue*/ );
       		
			lr[i].SetColors(c1, c2);
            lr[i].SetPosition(0, screenLoc);
			lr[i].SetPosition(1, screenLoc1);
        }
		
	}
	/*Renders the Transparent original triangle which acts as a reference for the normal startup state of the game*/
	void DrawShadowTriangle(LineRenderer[] lr){
				
		Vector3 screenLoc;
		Vector3 screenLoc1;
		
		//Draw shadow triangle, should refactor "v0dx..." etc. vars..
		/*for(int i = 0 ; i < NRATT ; i ++){*/
		screenLoc = cam.ScreenToWorldPoint(new Vector3(v0dx,v0dy,nearClipPlane));
		screenLoc1 = cam.ScreenToWorldPoint(new Vector3(v1dx,v1dy,nearClipPlane));
        lr[3].SetPosition(0, screenLoc);
		lr[3].SetPosition(1, screenLoc1);
	
		screenLoc = cam.ScreenToWorldPoint(new Vector3(v2dx,v2dy,nearClipPlane));
		screenLoc1 = cam.ScreenToWorldPoint(new Vector3(v1dx,v1dy,nearClipPlane));
        lr[4].SetPosition(0, screenLoc1);
		lr[4].SetPosition(1, screenLoc);
	
		screenLoc = cam.ScreenToWorldPoint(new Vector3(v2dx,v2dy,nearClipPlane));
		screenLoc1 = cam.ScreenToWorldPoint(new Vector3(v0dx,v0dy,nearClipPlane));
        lr[5].SetPosition(0, screenLoc);
		lr[5].SetPosition(1, screenLoc1);
		/*}*/		
	}
	
	//Maps the entity values to their corresponding values in the visualization
	float map(float a1,float a2,float b1,float b2,float s){
		return b1 + (s-a1)*(b2-b1)/(a2-a1);
	}
	
	/* Displays the icons (textures) on each vertex*/ 
	void OnGUI(){
		float texSize = 35; // size of texture to be displayed in pixels
		//display one texture at each vertex positon
		for(int i = 0; i < 3; i++){
			float texXPos = v[i].x - (texSize / 2);
			float texYPos = pixelRect.height -v[i].y - ((texSize / 2));
			GUI.DrawTexture(new Rect(texXPos, texYPos,texSize, texSize), icons[i], ScaleMode.ScaleToFit, true, 0f);
		}
		
		
	}
}
