using UnityEngine;
using System.Collections;

public class IT2_CannonShoot : MonoBehaviour, IT2_iCannonShoot {

public GameObject ammo;
	static public float turnSpeed = 0.5f;
	static public float pitchSpeed = 0.5f;
	
	public float turnTarget;
	public float pitchTarget;
	
	//Is cannon pitching OR turning?
	public bool isMoving = false;
	
	private bool isTurning = false;
	private bool isPitching = false;
	
	private float precision = 1.0f;
	
	private IT2_Player playerScript;
	
	private int playerNumber;
	
	//Variables for cooldown
	private bool canShoot = false;
	private float cooldownStartTime; 
	private int cooldownSet = 100000; //Set to a high value so it won't go off cooldown before the GameManager has had it's say.
	public int standardCooldown = 4;
	
	
	// Use this for initialization
	void Start () {
	}

	
	// Update is called once per frame
	void Update () {
		
		
		if((Time.realtimeSinceStartup - cooldownStartTime ) > cooldownSet){
				canShoot = true;
		}
		
		if(!isTurning && !isPitching){
			isMoving = false;
		}
		
		//Turn to the target
		if((Mathf.Abs(turnTarget - transform.eulerAngles.y) >= precision) && isTurning){
			isMoving = true;
			if(Mathf.DeltaAngle(transform.eulerAngles.y, turnTarget) >= 0)
				turn(turnSpeed);
			else
				turn (-turnSpeed);			
		}else{
			isTurning = false;	
		}
		
		//Adjust pitch of the cannon
		if((Mathf.Abs(pitchTarget - currentPitch()) >= precision) && isPitching){
			isMoving = true;
			if(Mathf.DeltaAngle(currentPitch(), pitchTarget) >= 0)
				pitch(-pitchSpeed);
			else
				pitch(pitchSpeed);
			
		}else{
			isPitching = false;	
		}

	}
	
	public void turnToTarget(float angle){
		if(angle >=0){
			turnTarget = angle%360;
		}else{
			turnTarget = 360 - Mathf.Abs(angle%360);
		}
		isTurning = true;
	}
	
	public void turn(float angle){
		if (this.playerScript.amOwner()){
			if (Network.isClient) {
				playerScript.GetComponent<NetworkView>().RPC("executeTurn", RPCMode.Server, angle);
			} else if (Network.isServer) {
				this.executeTurn(angle);
			}
		}
	}
	
	public void executeTurn(float angle) {
		transform.Rotate(0,angle,0);
	}
	
	public void pitchToTarget(float angle){
		if(angle >=0){
			pitchTarget = angle%360;
		}else{
			pitchTarget = 360 - Mathf.Abs(angle%360);
		}
		isPitching = true;
		
	}
	
	public void setPlayer(IT2_Player player) {
		this.playerScript = player;
	}
	
	[RPC]
	public void setPlayerNumber(int number) {
		this.playerNumber = number;
	}
	
	[RPC]
	public void setPosition(Vector3 position) {
		this.transform.position = position;
	}
	
	public int getPlayerNumber() {
		return this.playerNumber;
	}
	
	public IT2_Player getPlayer(){
		return this.playerScript;
	}
	
	
	public void pitch(float angle){
		if (this.playerScript.amOwner()){
			if (Network.isClient) {
				playerScript.GetComponent<NetworkView>().RPC("executePitch", RPCMode.Server, angle);
			} else if (Network.isServer) {
				this.executePitch(angle);
			}
		}
	}
	
	public void executePitch(float angle) {
		transform.FindChild("Sphere001").transform.Rotate(angle,0,0);
	}
	
	public void shoot(float power){
		if (this.playerScript.amOwner()){
			if (Network.isClient) {
				playerScript.GetComponent<NetworkView>().RPC("executeShoot", RPCMode.Server, power);
			} else if (Network.isServer) {
				this.executeShoot(power);
			}
		}
	}
	
	public void executeShoot(float power) {
		if(canShoot){
			GameObject shot = (GameObject)Network.Instantiate(ammo, transform.FindChild("Sphere001").position, transform.FindChild("Sphere001").rotation,0);
			shot.rigidbody.velocity = shot.transform.forward*power;
			
			//Set cannonballs to despawn when going 500 units from origo
			Bounds b = new Bounds(new Vector3(0,0,0), new Vector3(500,500,500));
			shot.GetComponent<IT2_SelfDestruct>().setDespawnBounds(b);
			
			GameObject.Find("CanonSound").audio.Play();
			
			setCooldown(standardCooldown);
		}
	}
	
	public void setCooldown(int s){
		canShoot = false;
		cooldownSet = s;
		cooldownStartTime = Time.realtimeSinceStartup;
	}
	
	public bool isReady(){
		return canShoot;
	}
	
	//get the angle from 0-360. Ugly hack.
	float currentPitch(){
		//forward half
		if(Mathf.RoundToInt(transform.FindChild("Sphere001").localEulerAngles.y) == 180){
			//if 1-90
			if(transform.FindChild("Sphere001").eulerAngles.x >= 270){
				return 90 - (transform.FindChild("Sphere001").eulerAngles.x % 90);
			}
			//if 360 degrees
			else if(Mathf.RoundToInt( transform.FindChild("Sphere001").eulerAngles.x) == 0){
			//if 270 degrees	
			}
			else if(Mathf.RoundToInt( transform.FindChild("Sphere001").eulerAngles.x) == 90){
				return transform.FindChild("Sphere001").eulerAngles.x % 90 +270f;
			}
			//if 271-359
			else
				return  90 - (transform.FindChild("Sphere001").eulerAngles.x % 90) +270f;
		}		
		//back half
		if(Mathf.RoundToInt( transform.FindChild("Sphere001").eulerAngles.z) == 180){
			if(transform.FindChild("Sphere001").eulerAngles.x >= 270){
				return (transform.FindChild("Sphere001").eulerAngles.x % 90) +90f;
			}
			return (transform.FindChild("Sphere001").eulerAngles.x % 90) + 180f;
				
		}
			
		return 0;
		
		
	}
}
