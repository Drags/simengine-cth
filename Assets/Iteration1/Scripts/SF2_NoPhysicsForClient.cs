using UnityEngine;
using System.Collections;

public class SF2_NoPhysicsForClient : MonoBehaviour {

	private Vector3 lastPosition;
	private Quaternion lastRotation;
	const float epsilon = 0.00000001f;
	// Use this for initialization
	void Start () {
		lastPosition = Vector3.zero;
		lastRotation = Quaternion.identity;
		if (Network.isClient) {
			rigidbody.isKinematic = true;
		}
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) {
		bool requiresUpdate = false;
		if (stream.isWriting) {
			if (!floatEquality(lastPosition.x, transform.position.x)){
				lastPosition.x = transform.position.x;
				requiresUpdate = true;
			}
			if (!floatEquality(lastPosition.y, transform.position.y)) {
				lastPosition.y = transform.position.y;
				requiresUpdate = true;
			}
			if (!floatEquality(lastPosition.z, transform.position.z)) {
				lastPosition.z = transform.position.z;
				requiresUpdate = true;
			}
			if (!floatEquality(lastRotation.x, transform.rotation.x)) {
				lastRotation.x = transform.rotation.x;
				requiresUpdate = true;
			}
			if (!floatEquality(lastRotation.y, transform.rotation.y)) {
				lastRotation.y = transform.rotation.y;
				requiresUpdate = true;
			}
			if (!floatEquality(lastRotation.z, transform.rotation.z)) {
				lastRotation.z = transform.rotation.z;
				requiresUpdate = true;
			}
			if (!floatEquality(lastRotation.w, transform.rotation.w)) {
				lastRotation.w = transform.rotation.w;
				requiresUpdate = true;
			}
			if (requiresUpdate) {
				Quaternion baseRotation = transform.rotation;
				Quaternion sphereRotation = transform.FindChild("Sphere001").rotation;
				stream.Serialize(ref baseRotation);
				stream.Serialize(ref sphereRotation);
			} else {
				return;
			}
		} else {
			Quaternion recBaseRotation = Quaternion.identity;
			Quaternion recSphereRotation = Quaternion.identity;
			stream.Serialize(ref recBaseRotation);
			stream.Serialize(ref recSphereRotation);
			transform.rotation = recBaseRotation;
			transform.FindChild("Sphere001").rotation = recSphereRotation;
		}
	}
	
	private bool floatEquality(float left, float right) {
		return (Mathf.Abs(right-left) < epsilon);
	}
}
